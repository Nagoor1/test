package com.GA_ChatBOT.HttpResponce;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class EmailSender {
	public void sendTestResults(String host, String port, final String userName, final String password,
			String toAddress, String message,  String[] attachFiles) throws AddressException, MessagingException, InterruptedException 
	{
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.user", userName);
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {
	        Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(userName));
	        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
	        msg.setRecipients(Message.RecipientType.TO, toAddresses);
	        msg.setSubject("GA ChatBOT Security Test Results");
	        msg.setSentDate(new Date());
	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(message, "text/html");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        if (attachFiles != null && attachFiles.length > 0){
	            for (String filePath : attachFiles) {
	                MimeBodyPart attachPart = new MimeBodyPart();
	 
	                try {
	                    attachPart.attachFile(filePath);
	                } catch (IOException ex) {
	                    ex.printStackTrace();
	                }
	 
	                multipart.addBodyPart(attachPart);
	            }
	        }
	        msg.setContent(multipart);
	        Transport.send(msg);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	public void emailReport(){
		String host = "smtp.us.deloitte.com";
		String port = "25";
		String mailFrom = "Enter your mail ID";
		String password = "Enter your Password";
		String mailTo = "ksivasugunareddy@deloitte.com";
		String message = "<i>Hi,</i><br>";
		message += "<br />";
		message += "<b>please find the below attachments of security scan test results and Extent Report for GA ChatBOT</b><br>";
		message += "<br />";
		message += "<br />";
		message += "<font color=red>Thanks,</font>";
		message += "<br />";
		message += "<font color=red>Prakash</font>";
		
		String[] attachFiles = new String[2];
        attachFiles[0] = System.getProperty("user.dir")+"\\Test\\FinalAPIReport.xls";
        attachFiles[1] = System.getProperty("user.dir")+"\\ExtentReport\\GA_ChatBOT_TestReport.html";
        
		try {
			sendTestResults(host, port, mailFrom, password, mailTo, message, attachFiles);
			System.out.println("Email sent");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}

	public void sendGmailTestResults(String host, String port, final String userName, final String password,
			String toAddress, String message,  String[] attachFiles) throws AddressException, MessagingException, InterruptedException 
	{

		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.user", userName);
		Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, password);
			}
		});

		try {
	        Message msg = new MimeMessage(session);
	        msg.setFrom(new InternetAddress(userName));
	        InternetAddress[] toAddresses = { new InternetAddress(toAddress) };
	        msg.setRecipients(Message.RecipientType.TO, toAddresses);
	        msg.setSubject("GA ChatBOT Test Results");
	        msg.setSentDate(new Date());
	        MimeBodyPart messageBodyPart = new MimeBodyPart();
	        messageBodyPart.setContent(message, "text/html");
	        Multipart multipart = new MimeMultipart();
	        multipart.addBodyPart(messageBodyPart);
	        
	        if (attachFiles != null && attachFiles.length > 0){
	            for (String filePath : attachFiles) {
	                MimeBodyPart attachPart = new MimeBodyPart();
	 
	                try {
	                    attachPart.attachFile(filePath);
	                } catch (IOException ex) {
	                    ex.printStackTrace();
	                }
	 
	                multipart.addBodyPart(attachPart);
	            }
	        }
	        msg.setContent(multipart);
	        Transport.send(msg);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void emailReport_Gmail() {
		APIResponceConverter apiRespConverter = new APIResponceConverter();
		String host = "smtp.gmail.com";
		String port = "587";
		String mailFrom = "Enter your mail ID";
		String password = "Enter you Password";
		String mailTo = "prakashqa.soft@gmail.com";

		String message = "<i>Hi,</i><br>";
		message += "<br />";
		message += "<b>please find the below attachment of security scan test results for GA ChatBOT</b><br>";
		message += "<br />";
		message += "<font color=red>Thanks,</font>";
		message += "<br />";
		message += "<font color=red>Prakash</font>";
		String[] attachFiles = new String[2];
		apiRespConverter.emailAttachments(attachFiles);
		/*String[] attachFiles = new String[2];
        attachFiles[0] = System.getProperty("user.dir")+"\\Test\\FinalAPIReport.xls";
        attachFiles[1] = System.getProperty("user.dir")+"\\ExtentReport\\GA_ChatBOT_TestReport.html";*/
        
		try {
			sendGmailTestResults(host, port, mailFrom, password, mailTo, message, attachFiles);
			System.out.println("Email sent");
		} catch (Exception ex) {
			System.out.println("Failed to sent email.");
			ex.printStackTrace();
		}
	}
}
